FROM adoptopenjdk/openjdk11:alpine-jre
VOLUME /tmp
ADD /target/*-with-dependencies.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]